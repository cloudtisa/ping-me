/*The MIT License (MIT)

Copyright (c) <2014> <Francis Maina>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. 

*/
var http = require('http');
var fs = require('fs');
var green_pass = 0; //counter for get requests that do not fail
var red_pass = 0; // counter for get requests that do fail
var http = require('http');


http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end("Green HTTP Get is :"+ green_pass +" and Failed HTTP Get is:" + red_pass);
}).listen(8080, '127.0.0.1');
console.log('Server running at http://127.0.0.1:8080/');


setInterval(function() {
	http.get("http://www.facebook.com", 
	
	function(res) 
	
	{
		green_pass += 1
		res.destroy() //end request because we dont want to keep the connection open
	}
	).on('error',
	function(e) 
	{ 
		red_pass += 1//log the error counters
		res.destroy() 
	}
	
	)},2000);

	
